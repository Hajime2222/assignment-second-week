const giveItBackLater = (value, callback) => {
  setTimeout(callback, 500, value)
}

const promiseToGiveItBackLater = value => new Promise(resolve =>
  giveItBackLater(value, resolve)
)

const addSomePromises = promise => promise
  .then(val => val.repeat(2))
  .catch(e => e.repeat(3))

module.exports = {
  giveItBackLater,
  promiseToGiveItBackLater,
  addSomePromises
}
