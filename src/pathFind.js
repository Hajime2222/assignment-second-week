function pathFind(path, object) {
  const nextPath = path.filter((el, i) => i !== 0)

  if (nextPath.length) return pathFind(nextPath, object[path[0]])
  return object[path[0]]
}

module.exports = { pathFind }
