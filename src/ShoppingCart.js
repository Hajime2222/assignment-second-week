class ShoppingCart {
  constructor(items = []) {
    this.items = items
  }
  getItems() {
    return this.items
  }
  addItem(itemName, quantity, price) {
    this.items.push({
      name: itemName,
      quantity,
      pricePerUnit: price
    })
  }
  clear() {
    this.items = []
  }
  clone() {
    const items = this.getItems().map(item => ({ ...item }))
    return new ShoppingCart(items)
  }
}

module.exports = ShoppingCart
